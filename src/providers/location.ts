import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Geolocation }  from 'ionic-native';

@Injectable()
export class Location {
  location :any;
  lat: any;
  lng : any;
  constructor(public http: Http) {
    
  }

getLocation(){
Geolocation.getCurrentPosition().then(resp => {
      //  console.log("these are my Coordinates");
       // console.log(resp.coords.latitude);
        this.lat = resp.coords.latitude;
        this.lng = resp.coords.longitude;

        this.location ={
          lat : this.lat ,
          lng : this.lng
        }
  }).then(() =>{
    return this.location;
  })
}
}