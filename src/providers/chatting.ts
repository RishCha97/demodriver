import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import PouchDB from 'pouchdb';

@Injectable()
export class Chatting {
    db: any; //database variable
    remote: any; //variable to store remote variable
    data: any;
    cloudantUsername: string;
    cloudantPassword: string;
    users = []; //all the anonymous users
    message:any ;
    constructor(public http: Http) {
        this.db = new PouchDB('chats');
        this.cloudantUsername = "testarefournficeirdshned";
        this.cloudantPassword = "cc3c8d20f509ed89ec22983227a03ad1a1a87885";
        this.remote = 'https://localhost:5984/chats';

        // this.cloudantUsername="";
        // this.cloudantPassword="";
        // this.remote="";
        let options = {
            live: true,
            retry: true,
            continous: true
        };
        this.db.sync(this.remote, options);
    }

    /**
     * Function for putting messages
     */

    addDocument(message) {
        let chat = {
            _id: new Date().toISOString(),
            message: message.message,
            to: message.toName,
            from: message.from,
            statusNew: true,
            date: new Date().getHours() + ":" + new Date().getMinutes()
        };
        this.db.put(chat); //putting message to server
        //console.log(chat);
        // this.getDocuments(message);
    }

    update(data){
        let message ={
            _id: data._id,
            _rev:data._rev,
            to:data.to,
            from:data.from,
            statusNew:data.statusNew,
            message:data.message,
            date: data.date
        }
        this.db.put(message);
    }
    /**
     * Function for reading messages for given reciever and sender
     */

    getDocuments(message) {

        return new Promise(resolve => {
            this.db.allDocs({


                include_docs: true,
                limit: 30,
                descending: true

            }).then((result) => {

                this.data = [];

                result.rows.map((row) => {
                    // console.log(row.doc);
                    // console.log(message);
                    if ((row.doc.to == message.toName && row.doc.from == message.from) || (row.doc.to == message.from && row.doc.from == message.toName)) {
                        this.data.push(row.doc);
                    }
                });

                this.data.reverse();

                resolve(this.data);

                this.db.changes({
                    live: true,
                    since: 'now',
                    include_docs: true
                }).on('change', (change) => {
                    this.handleChange(change);
                });

            }).catch((error) => {

                console.log(error);

            });

        });

    }
    checkNewChat(data){
         return new Promise(resolve => {
            this.db.allDocs({

                include_docs: true,
                limit: 30,
                descending: true

            }).then((result) => {

                result.rows.map((row) => {
                   // console.log(data.name)
                   // console.log(row.doc)
                    if(row.doc.to === data.name && row.doc.statusNew === true ){
                            this.message = true;
                    }
                });
                console.log(this.message);

             //   this.data.reverse();

                resolve(this.message);

                this.db.changes({
                    live: true,
                    since: 'now',
                    include_docs: true
                }).on('change', (change) => {
                    this.handleChange(change);
                });

            }).catch((error) => {

                console.log(error);

            });

        });

    }

    handleChange(change) {
        let changedDoc = null;
        let changedIndex = null;
        this.data.forEach((doc, index) => {

            if (doc._id === change.id) {
                changedDoc = doc;
                changedIndex = index;
            }
        });

        if (change.deleted) { //A document was Deleted
            this.data.splice(changedIndex, 1);
        } else {
            if (changedDoc) { //A document was updated          
                this.data[changedIndex] = change.doc;
            } else { //A document was added
                this.data.push(change.doc);
            }
        }

    }

    getAllDocuments() {

        return new Promise(resolve => {
            this.db.allDocs({
                include_docs: true,
                descending: true

            }).then((result) => {

                this.data = [];

                result.rows.map((row) => {
                    // console.log(row.doc);
                    // console.log(message);
                    if (row.doc.ano == "1") {
                        this.data.push(row.doc);
                    }
                    let count = 0;
                    this.users.forEach((data) => {
                        if (data.from == row.doc.from) {
                            count = 1;
                        }
                    });
                    if (count == 0 && row.doc.ano == "1") {
                        this.users.push(row.doc);
                    }
                });

                this.data.reverse();
                // console.log("this is this.users");
                // console.log(this.users);
                resolve(this.users);

                // this.db.changes({ live: true, since: 'now', include_docs: true }).on('change', (change) => {
                //   this.handleChange(change);
                // });

            }).catch((error) => {

                console.log(error);

            });

        });

    }

    getAnoDocuments(message) {

        return new Promise(resolve => {
            this.db.allDocs({


                include_docs: true,
                limit: 30,
                descending: true

            }).then((result) => {

                this.data = [];

                result.rows.map((row) => {
                    //console.log(row.doc);
                    // console.log(message);
                    if (((row.doc.toName == message.from) || (row.doc.from == message.from)) && row.doc.ano == '1') {
                        this.data.push(row.doc);
                    }
                });

                this.data.reverse();

                resolve(this.data);

                this.db.changes({
                    live: true,
                    since: 'now',
                    include_docs: true
                }).on('change', (change) => {
                    this.handleChange(change);
                });

            }).catch((error) => {

                console.log(error);

            });

        });

    }

     getNewDocuments(message) {

        return new Promise(resolve => {
            this.db.allDocs({

                include_docs: true,
                limit: 30,
                descending: true

            }).then((result) => {

                this.data = [];

                result.rows.map((row) => {
                    // console.log(row.doc);
                    // console.log(message);
                    if ((row.doc.to == message.from && row.doc.from == message.toName) && row.doc.statusNew == true) {
                        this.data.push(row.doc);
                    }
                });

                this.data.reverse();
                if(this.data[0] != undefined){
                resolve(this.data);
                }
            }).catch((error) => {

                console.log(error);

            });

        });

    }

}