import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class Geocoder {
  url: string = "https://maps.googleapis.com/maps/api/geocode/json?";
  api : string ="AIzaSyCwExBgDAlomkgKljSzR3FhFf1_lQiuk10";
  data: any;
  constructor(public http: Http) {
   
  }
FindCity(lat, lng){
  let url = this.url;
  url += "latlng=";
  url += lat+ ",";
  url += lng +"&key=";
  url += this.api;

  this.http.get(url)
   .subscribe(data =>{
     console.log(data.json().results);
   });
  //  err=>observer.error(err),
  //  ()=>observer.complete();;

}
}
