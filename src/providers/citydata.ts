import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import PouchDB from 'pouchdb';

@Injectable()
export class Citydata {
    db: any;
    remote: any; //variable to store remote variable
    data: any;
    cloudantUsername: string;
    cloudantPassword: string;

    constructor(public http: Http) {
        this.db = new PouchDB('cityData');
        this.cloudantUsername = "onsweenceplapediveramost";
        this.cloudantPassword = "e418e5c04b11b05f7711a23234fb4d47f35344fc";
        this.remote = 'http://localhost:5984/citydata';

        let options = {
            live: true,
            retry: true,
            continous: true
        };
        this.db.sync(this.remote, options);
    }

    getCities(){
            return new Promise(resolve => {

            this.db.allDocs({
                include_docs: true,
                descending: true
            }).then((result) => {

                this.data = [];
                let flag = 0;
                
                result.rows.map((row) => {
                        this.data.push(row.doc);
                });
                
                this.data.reverse();

                resolve(this.data);

                this.db.changes({
                    live: true,
                    since: 'now',
                    include_docs: true
                }).on('change', (change) => {
                    this.handleChange(change);
                });

            }).catch((error) => {

                console.log(error);

            });

        });
    }

    addToCities(city) {

        return new Promise(resolve => {

            this.db.allDocs({
                include_docs: true,
                descending: true
            }).then((result) => {

                this.data = [];
                let flag = 0;

                //flag for weather to add the city or not
                
                result.rows.map((row) => {
                    if (row.doc.city == city) {
                        flag = 1;
                    }
                });
                if (flag == 0) {
                    this.data.push(city);
                }
                this.data.reverse();

                // Making JSON object data to be stored on the Database
                let cityData = {
                    _id: new Date().toISOString(),
                    city: city,
                    users : []
                }
                this.db.put(cityData);
                resolve(this.data);

                this.db.changes({
                    live: true,
                    since: 'now',
                    include_docs: true
                }).on('change', (change) => {
                    this.handleChange(change);
                });

            }).catch((error) => {

                console.log(error);

            });

        });

    }

    handleChange(change) {
        let changedDoc = null;
        let changedIndex = null;
        this.data.forEach((doc, index) => {

            if (doc._id === change.id) {
                changedDoc = doc;
                changedIndex = index;
            }
        });

        if (change.deleted) { 
            //A document was Deleted
            this.data.splice(changedIndex, 1);
        } else {
            if (changedDoc) { 
                //A document was updated          
                this.data[changedIndex] = change.doc;
            } else { 
                //A document was added
                this.data.push(change.doc);
            }
        }

    }

    addUserDataToCities(userData){

        return new Promise(resolve => {

            this.db.allDocs({
                include_docs: true,
                descending: true
            }).then((result) => {

                this.data = [];
                let flag = 1;

                //flag for weather to add the city or not
                
                result.rows.map((row) => {
                    if (userData.cities[row.doc.city]==true) {
                        for(let i in row.doc.users){
                            if(row.doc.users[i] == userData.name){
                                flag = 0;
                            }
                        }
              if (flag == 1) {
                    row.doc.users.push(userData.name);
                    console.log(row.doc);
                    this.data.push[row.doc];
                    this.db.put(row.doc);
                }
                    }
                });
                
                this.data.reverse();
                resolve(this.data);

                this.db.changes({
                    live: true,
                    since: 'now',
                    include_docs: true
                }).on('change', (change) => {
                    this.handleChange(change);
                });

            }).catch((error) => {

                console.log(error);

            });

        });

    }

}