import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import PouchDB from 'pouchdb';

@Injectable()
export class DriverData {
    //Member Variables
    remote: any; //variable for storing remote database address
    db: any; //variable for creating database
    data: any; //this is used for simply taking data from server
    fix: any; //this variable is used to access data in addDrivers function to check for existing driver with same credentials
    cloudantUsername: string;
    cloudantPassword: string;
    options: any;
    constructor(public http: Http) {
        this.db = new PouchDB('DriverList');
        this.cloudantUsername = "maysedsonsolutdayswerani";
        this.cloudantPassword = "bc9d12fbb8e2cc03723e84cd91af9f9ce997175f";
        this.remote = 'https://6323cd5c-ab7a-4f85-8e7f-20a6d97305aa-bluemix.cloudant.com/driverdata';
        this.sync();

        this.getAllDrivers();
    }

    /**
     * function to syncronise data
     */

    sync() {
        this.options = {
            live: true,
            retry: true,
            continous: true,
            auth: {
                username: this.cloudantUsername,
                password: this.cloudantPassword
            }
        };
        this.db.sync(this.remote, this.options);

    }

    /**
     * function fetching data and checking for whether any username is taken or not
     */

    addDriver(DriverData) {


        return new Promise(resolve => {
            this.db.allDocs({

                include_docs: true,
                limit: 30,
                descending: true

            }).then((result) => {

                let count = 0; //flag for username exists(1) or not(0)
                // console.log(this.fix[0].username);
                for (let i = 0; i < this.fix.length; i++) {
                    //  console.log(this.fix[i].username);
                    if (this.fix[i].username == DriverData.username) {
                        count = 1;
                        //   console.log("count changed");
                    } else if (this.fix[i].name == DriverData.name && this.fix[i].city == DriverData.city) {
                        count = 1;
                    }
                }
                if (count === 0) {
                    this.db.put(DriverData);
                    alert("SuccessFully Registered, Please Login");
                }
                this.db.changes({
                    live: true,
                    since: 'now',
                    include_docs: true,
                    _rev: this
                }).on('change', (change) => {
                    this.handleChange(change);
                });
            }).catch((error) => {

                console.log(error);

            });

        });


    }

    /**
     * Function used for fetching data according to given credentials
     */

    getDriver(data) {
        // console.log("inside getDriver");
        return new Promise(resolve => {
            this.data = false;
            // console.log(this.data);
            this.db.allDocs({
                include_docs: true,
                // limit: 30,
                descending: true
            }).then((result) => {
                this.data = [];
                //   console.log(result);
                result.rows.map((row) => {
                    //    console.log("checking row.doc " + row.doc.username);
                    if (row.doc.username === data.username && row.doc.password === data.password) {
                        this.data = [];
                        row.doc.online = true;
                        this.data.push(row.doc);
                        //    console.log(this.data);
                        //  console.log(this.data);
                        resolve(this.data);
                    }
                    // else{
                    //   this.data = false;
                    //   console.log(i++);
                    // }
                });

                this.data.reverse();
                //  resolve(this.data);

                this.db.changes({
                    live: true,
                    since: 'now',
                    include_docs: true
                }).on('change', (change) => {
                    //console.log(change);
                    this.handleChange(change);

                });

            }).catch((error) => {

                console.log(error);

            });

        });
    }

    handleChange(change) { //change is whole data with some or more than one fields updated or added or deleted

        let changedDoc = null;
        let changedIndex = null;
        this.fix.forEach((doc, index) => {
            //console.log(doc._id === change.id);
            //console.log(change);
            // console.log(change.id);
            // console.log(doc._id);
            if (doc._id === change.id) {
                changedDoc = doc;
                changedIndex = index;
            }
        });

        if (change.deleted) { //A document was Deleted
            this.fix.splice(changedIndex, 1);
        } else {
            if (changedDoc) { //A document was updated          
                this.fix[changedIndex] = change.doc;
                // console.log(change.doc);
            } else { //A document was added
                this.fix.push(change.doc);
            }
        }

    }
    /*
     * retrievs data of all the drivers
     * 
     */

    getAllDrivers() {
        return new Promise(resolve => {

            this.db.allDocs({

                include_docs: true,
                //limit: 30,
                descending: true

            }).then((result) => {
                this.data = [];
                result.rows.map((row) => {
                    //console.log("checking row.doc " + row.doc.username);
                    if (row.doc.online == true) {
                        this.data.push(row.doc);
                    }
                    //console.log(this.data[0].username);
                });


                this.fix = this.data;
                resolve(this.data);

                this.db.changes({
                    live: true,
                    since: 'now',
                    include_docs: true
                }).on('change', (change) => {
                    this.handleChange(change);
                });

            }).catch((error) => {

                console.log(error);

            });

        });
    }
    /**
     * function to get driver data with name
     */
    getDriverFromName(name){

        return new Promise(resolve => {
            this.data = false;
            // console.log(this.data);
            this.db.allDocs({
                include_docs: true,
                // limit: 30,
                descending: true
            }).then((result) => {
                this.data = [];
                result.rows.map((row) => {
                        console.log(row.doc);
                    if (row.doc.name === name) {
                        resolve(row.doc);
                    }
                });

                this.data.reverse();

                this.db.changes({
                    live: true,
                    since: 'now',
                    include_docs: true
                }).on('change', (change) => {
                    //console.log(change);
                    this.handleChange(change);

                });

            }).catch((error) => {

                console.log(error);

            });

        });

    }

    /**
     * updatea the given doc
     */

    updateDoc(data) {
        return new Promise(() => {
            this.db.put(data);
        });
    }

    postDoc(data) {
         return new Promise(() => {
            this.db.allDocs({

                include_docs: true,
                //limit: 30,
                descending: true

            }).then((result) => {
                this.data = [];
                result.rows.map((row) => {
                    //console.log("checking row.doc " + row.doc.username);
                    if (row.doc._id == data._id) {
                        row.doc.cities = data.cities
                        this.db.post(row.doc);
                    }
                    //console.log(this.data[0].username);
                });

                this.db.changes({
                    live: true,
                    since: 'now',
                    include_docs: true
                }).on('change', (change) => {
                    this.handleChange(change);
                });

            }).catch((error) => {

                console.log(error);

            });
            // this.db.get(data._id).then(function(doc){
            //   doc.online =false;
            //   console.log(doc);
            //   console.log(doc.online);
            //    this.db.put(doc);
            // })
            // .then(function(){
            //   return this.db.get(data._id);
            // }).then(function(doc){
            //     console.log(doc);
            // })
        });
    }

    changeDoc(data) {
        return new Promise(() => {
            this.db.allDocs({

                include_docs: true,
                //limit: 30,
                descending: true

            }).then((result) => {
                this.data = [];
                result.rows.map((row) => {
                    //console.log("checking row.doc " + row.doc.username);
                    if (row.doc._id == data._id) {
                        row.doc.online = false;
                        this.db.post(row.doc);
                    }
                    //console.log(this.data[0].username);
                });

                this.db.changes({
                    live: true,
                    since: 'now',
                    include_docs: true
                }).on('change', (change) => {
                    this.handleChange(change);
                });

            }).catch((error) => {

                console.log(error);

            });
            // this.db.get(data._id).then(function(doc){
            //   doc.online =false;
            //   console.log(doc);
            //   console.log(doc.online);
            //    this.db.put(doc);
            // })
            // .then(function(){
            //   return this.db.get(data._id);
            // }).then(function(doc){
            //     console.log(doc);
            // })
        });
    }


    getAllCities(data) {
        return new Promise(resolve => {

            this.db.allDocs({
                include_docs: true,
                //limit: 30,
                descending: true
            }).then((result) => {
                let newData = [];
                result.rows.map((row) => {
                    //console.log("checking row.doc " + row.doc.username);
                    //console.log("checking  " + data[0].username);
                    console.log(row.doc.username != data[0].username);
                    //  if(row.doc.username != data[0].username){
                    let flag = 0;
                    for (let i of newData) {
                        if (i.city == row.doc.city) {
                            flag = 1;
                        }
                    }
                    if (flag == 1 && row.doc.online == true) {
                        newData.push(row.doc);
                        console.log(newData);
                    }
                    //console.log(this.data[0].username);
                    //  }
                });


                this.fix = newData;
                resolve(newData);

                this.db.changes({
                    live: true,
                    since: 'now',
                    include_docs: true
                }).on('change', (change) => {
                    this.handleChange(change);
                });

            }).catch((error) => {

                console.log(error);

            });

        });
    }
}