import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { YourDetailsPage } from '../pages/your-details/your-details';
import { LoginPage } from '../pages/login/login';
import { TaxiListPage } from '../pages/taxi-list/taxi-list';
import { ChatPage } from '../pages/chat/chat';
import { UserChatPage } from '../pages/user-chat/user-chat';
import { ChatUserPage }  from '../pages/chatuser/chat';
import { CityListPage } from '../pages/city-list/city-list';
import { SettingsPage } from '../pages/settings/settings';

import { Chatting } from '../providers/chatting';
import { DriverData } from '../providers/driver-data';
import { Geocoder } from '../providers/geocoder';
import { Location } from '../providers/location';
import { Citydata } from '../providers/citydata';

import { ShowHideContainer } from '../components/show-hide-password/show-hide-container';
import { ShowHideInput } from '../components/show-hide-password/show-hide-input';
import { PreloadImage } from '../components/preload-image/preload-image'

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TaxiListPage,
    ChatPage,
    YourDetailsPage,
    LoginPage,
    ShowHideContainer,
    ShowHideInput,
    PreloadImage,
    UserChatPage,
    ChatUserPage,
    CityListPage,
    SettingsPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TaxiListPage,
    ChatPage,
    YourDetailsPage,
    LoginPage,
    UserChatPage,
    ChatUserPage,
    CityListPage,
    SettingsPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler},Location, DriverData,  Chatting, Geocoder, Citydata ],
})
export class AppModule {}
