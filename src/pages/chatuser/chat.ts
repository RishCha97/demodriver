import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Chatting } from '../../providers/chatting';

@Component({
    selector: 'page-chat',
    templateUrl: 'chat.html'
})
export class ChatUserPage {
    data: any; //To whom chat is being done
    otherData: any = []; //Data about who is doing the chat, means YOU!!
    message: any;
    AllMessages: any = [];

    constructor(public navCtrl: NavController, public navParams: NavParams, public chat: Chatting) {

        //this.data = this.navParams.get('Reciever');  
        this.otherData = this.navParams.get('UserData');
        console.log("physics");
        console.log(this.otherData[0]);
        let thisChat = {
            from: this.otherData[0].name
        };
        // console.log(thisChat);
        this.chat.getAnoDocuments(thisChat).then((data) => {
            this.AllMessages = data;
            console.log(this.AllMessages);
        });
    }


    /**
     * message to submit the chat message
     */
    submit() {
        let message = {
            message: this.message,
            from: this.otherData[0].name
        };
        // console.log(message);
        this.chat.addDocument(message);
        this.message = '';
    }

}


