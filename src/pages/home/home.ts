import { Component } from '@angular/core';
import { DriverData } from '../../providers/driver-data';
import { NavController, NavParams } from 'ionic-angular';
import { Geolocation } from 'ionic-native';
import { Geocoder } from '../../providers/geocoder';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginPage } from '../login/login';
import { Citydata } from '../../providers/citydata'

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    DriverDetails: FormGroup;
    Data: any;
    variable: any;
    lat: any;
    lng: any;
    dataItem: any;



    constructor(public CityDataPro: Citydata, public geo: Geocoder, public formBuilder: FormBuilder, public navCtrl: NavController, public DriData: DriverData, public Nav: NavParams) {
        this.getLocation();
        //this.geo.FindCity(this.lat, this.lng);
        this.DriverDetails = formBuilder.group({
            name: ['', Validators.required],
            username: ['', Validators.required],
            city: ['', Validators.required],
            country: [''],
            password: ['', Validators.required],
            Confirm: ['', Validators.required]
        });
    }

    getLocation() {
        Geolocation.getCurrentPosition().then(resp => {
            this.lat = resp.coords.latitude;
            this.lng = resp.coords.longitude;
        });

    }

    getPlace() {

    }


    /**
     * for addition of new Drivers
     */
    SignUp() {
        this.getLocation();
        let dataToBeUploaded = {
            '_id': new Date().toISOString(), //preparing data to be uploaded
            'name': this.DriverDetails.value.name,
            'city': this.DriverDetails.value.city,
            'username': this.DriverDetails.value.username,
            'password': this.DriverDetails.value.password,
            'country': this.DriverDetails.value.country,
            'online': false,
            'lattitude': this.lat,
            'longitude': this.lng,
            'distance': ''
        };
        // console.log(this.DriverDetails.value);
        if (this.DriverDetails.value.Confirm == this.DriverDetails.value.password) {
            this.DriData.addDriver(dataToBeUploaded);
        } else {
            alert("Re-enter Credentials");
        }
        //this.SignIn();
        this.CityDataPro.addToCities(this.DriverDetails.value.city);
    }

}