import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Chatting } from '../../providers/chatting';
import { DriverData } from '../../providers/driver-data';

import { LoginPage } from '../login/login';
import { ChatPage } from '../chat/chat';

@Component({
    selector: 'page-user-chat',
    templateUrl: 'user-chat.html'
})
export class UserChatPage {
    users: any;
    Element1 = '';
    datas: any = [];
    constructor(public DriData: DriverData, public navCtrl: NavController, public navParams: NavParams, public chat: Chatting) {
        this.chat.getAllDocuments().then((data) => {
            this.users = data;
            this.datas = this.navParams.get('UserData');
            //console.log("michio kaku");
            //console.log(this.users);
        });
    }

    doChat(taxi) {
        this.navCtrl.push(ChatPage, {
            taxi: taxi
        });
    }

    LogOut() {
        this.datas[0].online = false;
        console.log("physics");
        console.log(this.datas[0]);
        let variable = this.datas[0];
        this.DriData.changeDoc(variable);
        //console.log(this.datas[0]._id);
        //console.log(this.datas[0]._rev);
        this.navCtrl.setRoot(LoginPage);
    }


}