import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Chatting } from '../../providers/chatting';

@Component({
    selector: 'page-chat',
    templateUrl: 'chat.html'
})
export class ChatPage {
    data: any; //To whom chat is being done
    otherData: any; //Data about who is doing the chat, means YOU!!
    message: any;
    AllMessages: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public chat: Chatting) {

        this.data = this.navParams.get('Reciever');
        this.otherData = this.navParams.get('Sender');

        let thisChat = {
            toName: this.data.name,
            from: this.otherData.name
        };
        this.chat.getDocuments(thisChat).then((data) => {
            for(let i in data){
            if(data[i].statusNew == true && data[i].to == this.data.name){
                data[i].statusNew = false;
                this.chat.update(data[i]);
               // console.log("update this")
               // console.log(data[i]);
            }
            }
            this.AllMessages = data;
            console.log(this.AllMessages[0]);
        });
    }


    /**
     * message to submit the chat message
     */
    submit() {
        let message = {
            message: this.message,
            toName: this.data.name,
            from: this.otherData.name
        };
        // console.log(message);
        this.chat.addDocument(message);
        this.message = '';
    }

}


