import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TaxiListPage } from '../taxi-list/taxi-list'
import { UserChatPage } from '../user-chat/user-chat';
import { ChatUserPage } from '../chatuser/chat';
import { DriverData } from '../../providers/driver-data';

@Component({
  selector: 'page-your-details',
  templateUrl: 'your-details.html'
})
export class YourDetailsPage {
  data : any =[];
  constructor(public DriData: DriverData,public navCtrl: NavController, public navParams: NavParams) {
    
      this.data = this.navParams.get('UserData');
}

/**
 * just forwards to available drivers
 */

Chats(){
    this.navCtrl.push(ChatUserPage , { UserData : this.data });
}

}
