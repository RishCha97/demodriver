import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DriverData } from '../../providers/driver-data';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { HomePage } from '../home/home';
import { CityListPage } from '../city-list/city-list';

@Component({
    selector: 'page-login',
    templateUrl: 'login.html'
})
export class LoginPage {

    dataItem: any;
    login: FormGroup;
    constructor(public navCtrl: NavController, public navParams: NavParams, public DriData: DriverData) {
        this.login = new FormGroup({
            username: new FormControl('', Validators.required),
            password: new FormControl('', Validators.required)
        });
    }


    SignIn() {
        let dataToBeSent = { //preparing data to be uploaded
            'username': this.login.value.username,
            'password': this.login.value.password
        };
        // console.log("data Fetching");
        this.DriData.getDriver(dataToBeSent).then((data) => {
            this.dataItem = data;
            this.dataItem.online = true;
            this.DriData.updateDoc(this.dataItem[0]);
        }).then(() => {
            //  console.log("data Fetched");
            if (this.dataItem) {
                this.navCtrl.setRoot(CityListPage, {
                    UserData: this.dataItem
                });
            }
        })
    }

    SignUpPage() {
        this.navCtrl.push(HomePage);
    }


}