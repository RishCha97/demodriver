import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { DriverData } from '../../providers/driver-data';

import { YourDetailsPage } from '../your-details/your-details';
import { TaxiListPage } from '../taxi-list/taxi-list';
import { LoginPage } from '../login/login';
import { ChatUserPage } from '../chatuser/chat';
import { SettingsPage } from '../settings/settings';
import { ChatPage } from '../chat/chat';

import { Citydata } from '../../providers/citydata';

@Component({
    selector: 'page-city-list',
    templateUrl: 'city-list.html'
})
export class CityListPage {
    data = [];
    checkBox = [];
    pass = [];
    constructor(public CityDataPro: Citydata, public navCtrl: NavController, public navParams: NavParams, public DriData: DriverData) {

    }
    ionViewDidEnter() {
        this.pass = this.navParams.get('UserData');
        this.getCityData(this.pass);
    }

    getCityData(datai) {
        this.data = [];
        this.CityDataPro.getCities().then((data) => {
            for (let i in data) {
                if (this.pass[0].cities[data[i].city] == true) {
                    this.data.push(data[i]);
                }
            }
            console.log(this.data);
        })

    }

    Logout() {
        this.pass[0].online = false;
        let variable = this.pass[0];
        this.DriData.changeDoc(variable);
        this.navCtrl.setRoot(LoginPage);
    }

    Submit() {
        this.navCtrl.push(TaxiListPage, {
            UserData: this.pass,
            CityData: this.checkBox
        });
    }
    Chats() {
        this.navCtrl.push(ChatUserPage, {
            UserData: this.pass
        });
    }

    Settings() {
        this.navCtrl.push(SettingsPage, {
            UserData: this.pass
        })
    }

    doChat(taxi) {

        this.DriData.getDriverFromName(taxi).then((Reciever) => {

            this.navCtrl.push(ChatPage, {
                Reciever: Reciever,
                Sender: this.pass[0]
            });

        })


    }



}