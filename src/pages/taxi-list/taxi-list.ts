import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Geolocation } from 'ionic-native';

import { DriverData } from '../../providers/driver-data';
import { Chatting } from '../../providers/chatting';

import { LoginPage } from '../login/login';
import { ChatPage } from '../chat/chat';


@Component({
    selector: 'page-taxi-list',
    templateUrl: 'taxi-list.html'
})
export class TaxiListPage {
    datas: any = []; //contains data of a single person who is logged in now (data of a person himself)
    taxis: any = []; //contains all other data
    NewData: any = []; //used in logout       
    data: any;
    locations: any;
    lat: any;
    lng: any;
    newData: any;
    Element: string = '';
    Element1: string = '';
    Cities;
    temp;
    message = [];
    show = [0];
    constructor(public chat : Chatting , public navCtrl: NavController, public navParams: NavParams, public DriData: DriverData) {

        this.datas = this.navParams.get('UserData');
        this.Cities = this.navParams.get('CityData');
        // console.log('this.cities');
        // console.log(this.Cities);
        //  console.log("this is this.datas");
        // console.log(this.datas);
        this.getDrivers();
      //  this.chat.getNewDocuments(this.datas);
        
    }

    getDrivers() {
        let temp = this;
        this.DriData.getAllDrivers().then((data) => {
            for (let i in data) {
                if (this.Cities[data[i].city]) {
                    console.log(data[i]);
                    this.taxis.push(data[i]);
                    
                   
                }
            }
            
           // this.chat.getNewDocuments(dat);
                 for(let j in this.taxis){
                     this.show[this.taxis[j].name]=0;
                     if(this.datas[0].name != this.taxis[j].name){
                     let dat ={
                        from: this.datas[0].name,
                        toName: this.taxis[j].name
                    };
                    this.getNewChats(dat);
                console.log(dat);
                     }
                 }
               // console.log(this.message[i]);
            console.log(this.message);
            console.log(this.show);

        },() => {
            this.location();
        }).then(() => {
            this.newData = this.load();
            // console.log("this is newData");
            // console.log(this.newData);
        })

    }

    getNewChats(data){
       
       this.chat.getNewDocuments(data).then((data)=>{
           for(let i in data){
               this.message.push(data[i]);
               this.show[data[i].from]++; 
           }
           
       })    
    }

    /**
     * this is the logout button and just changes the online property of a particular taxi to false
     */

    location() {
        Geolocation.getCurrentPosition().then(resp => {
            //console.log("these are my Coordinates");
            //console.log(resp.coords.latitude);
            this.lat = resp.coords.latitude;
            this.lng = resp.coords.longitude;
        }).then(() => {
            if (this.datas[0].lattitude != this.lat) {
                //console.log(this.datas[0].lattitude);
                this.datas[0].lattitude = this.lat;
                //console.log(this.datas[0].lattitude);
                this.datas[0].longitude = this.lng;
                this.DriData.changeDoc(this.datas[0]).then(() => {
                    this.DriData.getAllDrivers().then((data) => {
                        this.taxis = data;
                        // console.log("check thsi also");
                        //console.log(this.taxis);
                    });
                });
            }
            //this.DriData.sync();
        });

    }

    /**
     * Function used for searching Drivers by name
     */
    getItems(ev: any) {

        let value = ev.target.value;

        if (value && value.trim() != this.Element1) {
            this.Element1 = value;
            this.taxis = this.taxis.filter((item) => {
                return (item.city.toLowerCase().indexOf(value.toLowerCase()) > -1);
            })
        } else {
            this.getDrivers();
        }
    }

    LogOut() {
       // clearInterval(this.temp);
        this.datas[0].online = false;
        let variable = this.datas[0];
        this.DriData.changeDoc(variable);
        //console.log(this.datas[0]._id);
        //console.log(this.datas[0]._rev);
        this.navCtrl.setRoot(LoginPage);
    }


    doChat(taxi) {

       // clearInterval(this.temp);
        this.navCtrl.push(ChatPage, {
            Reciever: taxi,
            Sender: this.datas[0]
        });

    }

    /**
     * all the following functions are used to calculate distance
     */

    load() {

        // if(this.taxis){
        //     return Promise.resolve(this.taxis);
        // }

        return new Promise(resolve => {


            this.data = this.applyHaversine(this.taxis);
            //   console.log("check this");
            //  console.log(this.data);
            this.data.sort((locationA, locationB) => {
                //console.log(locationA.distance - locationB.distance);
                return locationA.distance - locationB.distance;
            });
            //console.log(this.data);
            // this.list=this.data;
            resolve(this.data);
        });


    }

    applyHaversine(locations) {
        // console.log(locations);

        let usersLocation = {
            lat: this.datas[0].lattitude,
            lng: this.datas[0].longitude
        };
        //console.log("this is usersLocation");
        // console.log(usersLocation);
        locations.map((location) => {
            // console.log(location);
            let placeLocation = {

                lat: location.lattitude,
                lng: location.longitude
            };

            // console.log(location.lattitude);
            location.distance = this.getDistanceBetweenPoints(
                usersLocation,
                placeLocation,
                'm'
            ).toFixed(8);
        });

        return locations;
    }

    getDistanceBetweenPoints(start, end, units) {

        let earthRadius = {
            miles: 3958.8,
            km: 6371,
            m: 6371000
        };

        let R = earthRadius[units || 'miles'];
        let lat1 = start.lat;
        let lon1 = start.lng;
        let lat2 = end.lat;
        let lon2 = end.lng;

        let dLat = this.toRad((lat2 - lat1));
        let dLon = this.toRad((lon2 - lon1));

        let a = (Math.sin(dLat / 2) * Math.sin(dLat / 2)) + (Math.cos(this.toRad(lat1)) * Math.cos(this.toRad(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2));

        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        let d = R * c;

        // console.log("he;llo"+d);
        return d;

    }

    toRad(x) {
        return x * Math.PI / 180;
    }


}