import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Citydata } from '../../providers/citydata';
import { DriverData } from '../../providers/driver-data';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {
  data = [];
  checkBox = [];
  pass:any;
  constructor(public DriData : DriverData ,public CityDataPro : Citydata ,public navCtrl: NavController, public navParams: NavParams) {
    this.pass = this.navParams.get('UserData');
    this.getCityData();
  }

  

  getCityData() {
        this.CityDataPro.getCities().then((data) => {
            for (let i in data) {
                  this.data.push(data[i].city);
            }
        })
    }


    Submit(){
        let Json = Object.assign({}, this.checkBox);
        this.pass[0].cities = Json;
        this.DriData.postDoc(this.pass[0]);
        this.CityDataPro.addUserDataToCities(this.pass[0]);
    }

}
