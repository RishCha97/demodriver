import { Component, ElementRef } from '@angular/core';
import { ShowHideInput } from './show-hide-input'

@Component({
  selector: 'show-hide-container',
  templateUrl: 'show-hide-password.html',
  host: {
    'class': 'show-hide-password'
  }
})
export class ShowHideContainer
{
  show = false;

  //@ContentChild(ShowHideInput) input: ShowHideInput;
input :ShowHideInput;
  constructor(public el : ElementRef){}

  toggleShow()
  {
   // console.log(this.input);
    this.show = !this.show;
    if (this.show){
      this.el.nativeElement.children[0].type ="text";
    }
    else {
      this.input.changeType("password");
    }
  }
}
